/**
 * DRIVERS.CPP - list of the drivers and their characteristics
 *
 * If you are changing number of drivers here, change also MAXCARS in car.h !!!
 * These are the control or "driver" programs which compete in the race:
 * See drivers[] below, and CAR.H
 *
 * @author    Mitchell E. Timin, State College, PA
 * @see:      C++ Coding Standard and CCDOC in help.htm
 * @version   0.76
 *
 * Geändert von Ingo Haschler für SE-Projekt ET09
 * ... und nochmals für ET10 (Nummerierung angepaßt)
 * ... und dann nochmals, um zwei Kurse zu unterstützen
 * ... und wieder für ET12 (neues Namensschema)
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"

//--------------------------------------------------------------------------
//                           G L O B A L S
//--------------------------------------------------------------------------
// for n in `seq -w 0 60`; do echo "robot2 getRobot${n}Instance;" ; done
robot2 getTutorial1Instance;
robot2 getRobot01Instance;
robot2 getRobot02Instance;
robot2 getRobot03Instance;
robot2 getRobot04Instance;
robot2 getRobot05Instance;
robot2 getRobot06Instance;
robot2 getRobot07Instance;
robot2 getRobot08Instance;
robot2 getRobot09Instance;
robot2 getRobot10Instance;
robot2 getRobot11Instance;
robot2 getRobot12Instance;
robot2 getRobot13Instance;
robot2 getRobot14Instance;
robot2 getRobot15Instance;
robot2 getRobot16Instance;
robot2 getRobot17Instance;
robot2 getRobot18Instance;
robot2 getRobot19Instance;
robot2 getRobot20Instance;
robot2 getRobot21Instance;
robot2 getRobot22Instance;
robot2 getRobot23Instance;
robot2 getRobot24Instance;
robot2 getRobot25Instance;
robot2 getRobot26Instance;
robot2 getRobot27Instance;
robot2 getRobot28Instance;
robot2 getRobot29Instance;
robot2 getRobot30Instance;
robot2 getRobot31Instance;
robot2 getRobot32Instance;
robot2 getRobot33Instance;
robot2 getRobot34Instance;
robot2 getRobot35Instance;
robot2 getRobot36Instance;
robot2 getRobot37Instance;
robot2 getRobot38Instance;
robot2 getRobot39Instance;
robot2 getRobot40Instance;
robot2 getRobot41Instance;
robot2 getRobot42Instance;
robot2 getRobot43Instance;
robot2 getRobot44Instance;
robot2 getRobot45Instance;
robot2 getRobot46Instance;
robot2 getRobot47Instance;
robot2 getRobot48Instance;
robot2 getRobot49Instance;
robot2 getRobot50Instance;
robot2 getRobot51Instance;
robot2 getRobot52Instance;
robot2 getRobot53Instance;
robot2 getRobot54Instance;
robot2 getRobot55Instance;
robot2 getRobot56Instance;
robot2 getRobot57Instance;
robot2 getRobot58Instance;
robot2 getRobot59Instance;
robot2 getRobot60Instance;

/**
 * This is the permanent array of available drivers.
 *
 * Each bitmap name is associated with 2 colors.
 */
Driver * drivers[] =
{
    ////////////////////////////////////////////////////////////////////////////
    //  ET cars
    ////////////////////////////////////////////////////////////////////////////
	// for n in `seq -w 0 60`; do echo "getRobot${n}Instance()," ; done */
getTutorial1Instance(),
getRobot01Instance(),
getRobot02Instance(),
getRobot03Instance(),
getRobot04Instance(),
getRobot05Instance(),
getRobot06Instance(),
getRobot07Instance(),
getRobot08Instance(),
getRobot09Instance(),
getRobot10Instance(),
getRobot11Instance(),
getRobot12Instance(),
getRobot13Instance(),
getRobot14Instance(),
getRobot15Instance(),
getRobot16Instance(),
getRobot17Instance(),
getRobot18Instance(),
getRobot19Instance(),
getRobot20Instance(),
getRobot21Instance(),
getRobot22Instance(),
getRobot23Instance(),
getRobot24Instance(),
getRobot25Instance(),
getRobot26Instance(),
getRobot27Instance(),
getRobot28Instance(),
getRobot29Instance(),
getRobot30Instance(),
getRobot31Instance(),
getRobot32Instance(),
getRobot33Instance(),
getRobot34Instance(),
getRobot35Instance(),
getRobot36Instance(),
getRobot37Instance(),
getRobot38Instance(),
getRobot39Instance(),
getRobot40Instance(),
getRobot41Instance(),
getRobot42Instance(),
getRobot43Instance(),
getRobot44Instance(),
getRobot45Instance(),
getRobot46Instance(),
getRobot47Instance(),
getRobot48Instance(),
getRobot49Instance(),
getRobot50Instance(),
getRobot51Instance(),
getRobot52Instance(),
getRobot53Instance(),
getRobot54Instance(),
getRobot55Instance(),
getRobot56Instance(),
getRobot57Instance(),
getRobot58Instance(),
getRobot59Instance(),
getRobot60Instance(),

};
