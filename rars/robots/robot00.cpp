/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"

//--------------------------------------------------------------------------
//                           Class Tutorial 1
//--------------------------------------------------------------------------

class Tutorial1 : public Driver
{
public:
    // Konstruktor
    Tutorial1()
    {
        // Der Name des Robots
        m_sName = "Tuto1";
        // Namen der Autoren
        m_sAuthor = "Lucky Luke";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBLUE;
        m_iTailColor = oBLUE;
        m_sBitmapName2D = "car_blue_blue";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

    con_vec drive(situation& s)
    {
        // hier wird die Logik des Robots implementiert
        con_vec result = CON_VEC_EMPTY;

        if( s.starting )
        {
            result.fuel_amount = MAX_FUEL;
        }

        if( stuck( s.backward, s.v, s.vn, s.to_lft, s.to_rgt, &result.alpha, &result.vc ) )
        {
            return result;
        }

        result.vc = 20;
        result.alpha = 0.0;
        result.request_pit = 0;

        return result;
    }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getTutorial1Instance()
{
    return new Tutorial1();
}
