/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"

//--------------------------------------------------------------------------
//                           Class Robot17
//--------------------------------------------------------------------------

class Robot17 : public Driver
{
public:
    // Konstruktor
    Robot17()
    {
        // Der Name des Robots
        m_sName = "Robot17";
        // Namen der Autoren: Christian Lang
        m_sAuthor = "";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oMAGENTA;
        m_iTailColor = oYELLOW;
        m_sBitmapName2D = "car_magenta_gelb";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

    con_vec drive(situation& s)
    {
        // hier wird die Logik des Robots implementiert



        if(s.cur_rad == 0.0)               // If we are on a straightaway,
        if(s.to_end > .4 * s.cur_len) // if we are far from the end,
        return.vc = s.v + 50.0;              // pedal to the metal!


    }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot17Instance()
{
    return new Robot17();
}
